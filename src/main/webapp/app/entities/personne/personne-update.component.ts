import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPersonne } from 'app/shared/model/personne.model';
import { PersonneService } from './personne.service';
import { IUser, UserService } from 'app/core';

@Component({
    selector: 'jhi-personne-update',
    templateUrl: './personne-update.component.html'
})
export class PersonneUpdateComponent implements OnInit {
    private _personne: IPersonne;
    isSaving: boolean;

    users: IUser[];
    dateNaissanceDp: any;

    constructor(
        private jhiAlertService: JhiAlertService,
        private personneService: PersonneService,
        private userService: UserService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ personne }) => {
            this.personne = personne;
        });
        this.userService.query().subscribe(
            (res: HttpResponse<IUser[]>) => {
                this.users = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.personne.id !== undefined) {
            this.subscribeToSaveResponse(this.personneService.update(this.personne));
        } else {
            this.subscribeToSaveResponse(this.personneService.create(this.personne));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPersonne>>) {
        result.subscribe((res: HttpResponse<IPersonne>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }
    get personne() {
        return this._personne;
    }

    set personne(personne: IPersonne) {
        this._personne = personne;
    }
}
