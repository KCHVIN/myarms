import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MyarmsSharedModule } from 'app/shared';
import { MyarmsAdminModule } from 'app/admin/admin.module';
import {
    PersonneComponent,
    PersonneDetailComponent,
    PersonneUpdateComponent,
    PersonneDeletePopupComponent,
    PersonneDeleteDialogComponent,
    personneRoute,
    personnePopupRoute
} from './';

const ENTITY_STATES = [...personneRoute, ...personnePopupRoute];

@NgModule({
    imports: [MyarmsSharedModule, MyarmsAdminModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PersonneComponent,
        PersonneDetailComponent,
        PersonneUpdateComponent,
        PersonneDeleteDialogComponent,
        PersonneDeletePopupComponent
    ],
    entryComponents: [PersonneComponent, PersonneUpdateComponent, PersonneDeleteDialogComponent, PersonneDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyarmsPersonneModule {}
