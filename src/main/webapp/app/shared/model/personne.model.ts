import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IPersonne {
    id?: number;
    nom?: string;
    prenom?: string;
    dateNaissance?: Moment;
    user?: IUser;
}

export class Personne implements IPersonne {
    constructor(public id?: number, public nom?: string, public prenom?: string, public dateNaissance?: Moment, public user?: IUser) {}
}
